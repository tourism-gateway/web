<?php

use App\Http\Controllers\Web\WebAdminDashboardController;
use App\Http\Controllers\Web\WebAdminRoleController;
use App\Http\Controllers\Web\WebAdminUserController;
use App\Http\Controllers\Web\WebAuthController;
use App\Http\Controllers\Web\WebBussinessController;
use App\Http\Controllers\Web\WebPositionController;
use App\Http\Controllers\Web\WebPegawaiController;
use App\Http\Controllers\Web\WebPackageController;
use App\Http\Controllers\Web\WebCompaniesController;
use App\Http\Controllers\Web\WebVendorsController;
use App\Http\Controllers\Web\WebItemVendorsController;
use App\Http\Controllers\Web\WebKategoriItemController;
use App\Http\Controllers\Web\WebKategoryAttributItemController;
use App\Http\Controllers\Web\WebInvoiceController;
use App\Http\Controllers\Api\ApiReservasiController;
use App\Http\Controllers\Web\WebReservasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Route::get('/login-form', [WebAuthController::class, 'loginView']);
Route::get('/', [WebAuthController::class, 'landingPage']);
Route::get('/reservasi', [WebAuthController::class, 'reservasi']);

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/auth/logout', [WebAuthController::class, 'logout']);
Route::get('/auth/login', [WebAuthController::class, 'loginView'])->name('login');
Route::post('/auth/login-prosses', [WebAuthController::class, 'loginProsses']);

Route::prefix('reservasi-post')->group(function () {
    Route::post('/', [ApiReservasiController::class, 'store']);//->middleware('permission:bussiness.show');
});

Route::middleware('auth')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('dashboard', [WebAdminDashboardController::class, 'index']);

        Route::get('positions', [WebPositionController::class, 'index']);
        //crud User
        Route::get('users', [WebAdminUserController::class, 'index'])->middleware('permission:user.show');

        //crud bussiness
        Route::prefix('bussiness')->group(function () {
            Route::get('/', [WebBussinessController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        //crud packages
        Route::prefix('packages')->group(function (){
            Route::get('/', [WebPackageController::class, 'index']);
        });

        Route::prefix('roles')->group(function () {
            Route::get('/', [WebAdminRoleController::class, 'index'])->middleware('permission:role.show');
            Route::get('create', [WebAdminRoleController::class, 'create'])->middleware('permission:role.create');
            Route::get('/{id}/edit', [WebAdminRoleController::class, 'edit'])->middleware('permission:role.edit');
        });

        Route::prefix('bussiness')->group(function () {
            Route::get('/', [WebBussinessController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('employes')->group(function () {
            Route::get('/', [WebPegawaiController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('company')->group(function () {
            Route::get('/', [WebCompaniesController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('vendors')->group(function () {
            Route::get('/', [WebVendorsController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('vendors-item')->group(function () {
            Route::get('/', [WebItemVendorsController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('category-item')->group(function () {
            Route::get('/', [WebKategoriItemController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('kategory-attribute-item')->group(function () {
            Route::get('/', [WebKategoryAttributItemController::class, 'index']);//->middleware('permission:bussiness.show');
        });

        Route::prefix('invoice')->group(function () {
            Route::get('/', [WebInvoiceController::class, 'index']);//->middleware('permission:bussiness.show');
            Route::get('/show/{id}', [WebInvoiceController::class, 'invoice'])->name('reservasi_invoice');
        });

        Route::prefix('reservasi')->group(function () {
            Route::get('/', [WebReservasiController::class, 'index']);//->middleware('permission:bussiness.show');
            Route::get('/create', [WebReservasiController::class, 'create']);//->middleware('permission:bussiness.show');
            Route::get('/detail/{id}', [WebReservasiController::class, 'detail']);//->middleware('permission:bussiness.show');
            Route::get('/view-invoice/{id}', [WebReservasiController::class, 'viewInvoice']);//->middleware('permission:bussiness.show');
        });

        
    });
});
