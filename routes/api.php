<?php

use App\Http\Controllers\Api\ApiAuthController;
use App\Http\Controllers\Api\ApiPositionController;
use App\Http\Controllers\Api\ApiPackageController;
use App\Http\Controllers\Api\ApiRoleController;
use App\Http\Controllers\Api\ApiUserController;
use App\Http\Controllers\Api\ApiBussinessController;
use App\Http\Controllers\Api\ApiPegawaiController;
use App\Http\Controllers\Api\ApiCompaniesController;
use App\Http\Controllers\Api\ApiVendorsController;
use App\Http\Controllers\Api\ApiItemVendorsController;
use App\Http\Controllers\Api\ApiKategoriItemController;
use App\Http\Controllers\Api\ApiReservasiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [ApiAuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {

    Route::prefix('admin')->group(function () {

        Route::prefix('roles')->group(function () {
            Route::get('/', [ApiRoleController::class, 'index'])->middleware('permission:role.show');
            Route::get('/all', [ApiRoleController::class, 'all'])->middleware('permission:role.show');
            Route::post('/', [ApiRoleController::class, 'store'])->middleware('permission:role.create');
            Route::get('/{id}/show', [ApiRoleController::class, 'show'])->middleware('permission:role.show');
            Route::put('{id}/update', [ApiRoleController::class, 'update'])->middleware('permission:role.edit');
            Route::delete('{id}/delete', [ApiRoleController::class, 'destroy'])->middleware('permission:role.delete');
        });

        Route::prefix('users')->group(function () {
            Route::get('/', [ApiUserController::class, 'index'])->middleware('permission:user.show');
            Route::post('/', [ApiUserController::class, 'store'])->middleware('permission:user.create');
            Route::put('{id}/update', [ApiUserController::class, 'update'])->middleware('permission:user.edit');
            Route::delete('{id}/delete', [ApiUserController::class, 'destroy'])->middleware('permission:user.delete');
        });

        Route::prefix('positions')->group(function () {
            Route::get('/', [ApiPositionController::class, 'index']);
            Route::post('/', [ApiPositionController::class, 'store']);
            Route::put('{id}/update', [ApiPositionController::class, 'update']);
            Route::delete('{id}/delete', [ApiPositionController::class, 'destroy']);
        });

        Route::prefix('bussiness')->group(function () {
            Route::get('/', [ApiBussinessController::class, 'index']);
            Route::post('/', [ApiBussinessController::class, 'store']);
            Route::put('{id}/update', [ApiBussinessController::class, 'update']);
            Route::delete('{id}/delete', [ApiBussinessController::class, 'destroy']);
        });
        
        Route::prefix('packages')->group(function () {
            Route::get('/', [ApiPackageController::class, 'index']);
            Route::post('/', [ApiPackageController::class, 'store']);
            Route::put('{id}/update', [ApiPackageController::class, 'update']);
            Route::delete('{id}/delete', [ApiPackageController::class, 'destroy']);
        });

        Route::prefix('employes')->group(function () {
            Route::get('/', [ApiPegawaiController::class, 'index']);
            Route::post('/', [ApiPegawaiController::class, 'store']);
            Route::put('{id}/update', [ApiPegawaiController::class, 'update']);
            Route::delete('{id}/delete', [ApiPegawaiController::class, 'destroy']);
        });

        Route::prefix('company')->group(function () {
            Route::get('/', [ApiCompaniesController::class, 'index']);
            Route::post('/', [ApiCompaniesController::class, 'store']);
            Route::put('{id}/update', [ApiCompaniesController::class, 'update']);
            Route::delete('{id}/delete', [ApiCompaniesController::class, 'destroy']);
        });

        Route::prefix('vendors')->group(function () {
            Route::get('/', [ApiVendorsController::class, 'index']);
            Route::post('/', [ApiVendorsController::class, 'store']);
            Route::put('{id}/update', [ApiVendorsController::class, 'update']);
            Route::delete('{id}/delete', [ApiVendorsController::class, 'destroy']);
        });

        Route::prefix('vendor-items')->group(function () {
            Route::get('/', [ApiItemVendorsController::class, 'index']);
            Route::post('/', [ApiItemVendorsController::class, 'store']);
            Route::put('{id}/update', [ApiItemVendorsController::class, 'update']);
            Route::delete('{id}/delete', [ApiItemVendorsController::class, 'destroy']);
        });

        Route::prefix('category-item')->group(function () {
            Route::get('/', [ApiKategoriItemController::class, 'index']);
            Route::post('/', [ApiKategoriItemController::class, 'store']);
            Route::put('{id}/update', [ApiKategoriItemController::class, 'update']);
            Route::delete('{id}/delete', [ApiKategoriItemController::class, 'destroy']);
        });

        Route::prefix('reservasi')->group(function () {
            Route::get('/select2-item', [ApiReservasiController::class, 'get_item']);
            Route::get('/datatables-item/{id}', [ApiReservasiController::class, 'datatablesItem']);
            Route::get('/datatables', [ApiReservasiController::class, 'index']);
            Route::delete('/delete-item/{id}', [ApiReservasiController::class, 'deleteItem']);
            Route::post('/store', [ApiReservasiController::class, 'storeReservasi']);
            Route::post('/add-new-item', [ApiReservasiController::class, 'addItem']);
        });
    });
});
