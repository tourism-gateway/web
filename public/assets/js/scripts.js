
function scroll_to(clicked_link, nav_height) {
	var element_class = clicked_link.attr('href').replace('#', '.');
	var scroll_to = 0;
	if(element_class != '.top-content') {
		element_class += '-container';
		scroll_to = $(element_class).offset().top - nav_height;
	}
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 1000);
	}
}


jQuery(document).ready(function() {
	
	/*
	    Navigation
	*/
	$('a.scroll-link').on('click', function(e) {
		e.preventDefault();
		scroll_to($(this), $('nav').outerHeight());
	});
	// toggle "navbar-no-bg" class
	$('.top-content .carousel-caption').waypoint(function() {
		$('nav').toggleClass('navbar-no-bg');
	});
	
    /*
        Background slideshow
    */
    $('.section-4-container').backstretch("assets/img/backgrounds/3.jpg");
    
    /*
	    Wow
	*/
	new WOW().init();
	
});

// Initialize and add the map
function initMap() {
	// The location of Uluru
	const highland = { lat: -6.650879, lng: 106.945277 };
	// The map, centered at Uluru
	const map = new google.maps.Map(document.getElementById("map"), {
		zoom: 12,
		center: highland,
	});
	// The marker, positioned at Uluru
	const marker = new google.maps.Marker({
		position: highland,
		map: map,
	});
	//Callout Content
	var contentString = '<a href="https://goo.gl/maps/1GrAe9buoSU93Qru9" target="_blank">HIGHLAND CAMP - for gathering, camping and outbound</a>';
	//Set window width + content
	var infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 500
	});
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});
}