<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>{{ $reservasi->code_invoice }} | Invoice</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition sidebar-mini">
    <div id="invoice">
        <table style="width: 100%">
            <tr>
                <td></td>
                <td class="text-right"><img src="{{ asset('astech.png') }}" alt="" width="10%"> </td>
            </tr>
            <tr>
                <td></td>
                <td class="text-center"><h4><strong>Invoice</strong></h4></td>
                <td></td>
            </tr>
        </table><br>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <table  style="width: 80%">
                            <tr>
                                <td><strong>From</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Name </strong></td>
                                {{-- <td>:</td> --}}
                                <td><Strong>HIGHLAND CAMP</Strong></td>
                            </tr>
                            <tr>
                                <td><strong>Address </strong></td>
                                {{-- <td>:</td> --}}
                                <td>Jl. Situhiang, Megamendung,<br> Kec. Megamendung, Bogor, Jawa Barat 16770</td>
                            </tr>
                            <tr>
                                <td><strong>City </strong></td>
                                {{-- <td>:</td> --}}
                                <td>Bogor</td>
                            </tr>
                            <tr>
                                <td><strong>Phone </strong></td>
                                {{-- <td>:</td> --}}
                                <td>0857-8000-2200</td>
                            </tr>
                        </table><br>
                    </div>
                    <div class="col-md-5" class="text-right">
                        <table  class="text-right" style="width:100%">
                            <tr>
                                <td>No Invoice</td>
                                <td>{{ $reservasi->code_invoice }}</td>
                            </tr>
                            <tr>
                                <td>Invoice Date</td>
                                <td>{{ date('M-d-Y', strtotime($reservasi->created_at)) }}</td>
                            </tr>
                            <tr>
                                <td>Term Of Payment</td>
                                <td>-</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        <table  style="width: 80%">
                            <tr>
                                <td><strong>To </strong></td>
                            </tr>
                            <tr>
                                <td><strong>Name </strong></td>
                                {{-- <td>:</td> --}}
                                <td><Strong>{{ $reservasi->user->nama }}</Strong></td>
                            </tr>
                            <tr>
                                <td><strong>Phone </strong></td>
                                {{-- <td>:</td> --}}
                                <td>{{ $reservasi->user->telepon }}</td>
                            </tr>
                            <tr>
                                <td><strong>Address </strong></td>
                                {{-- <td>:</td> --}}
                                <td>-</td>
                            </tr>
                            
                        </table><br>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <center>Paket</center><br>
        <table border="1" class="table" style="width:100%">
            <tr>
                <td>Nama Paket</td>
                <td>Harga</td>
                <td>PAX</td>
                <td>Total</td>
            </tr>
            <tbody>
                @php 
                    $totalPax = $reservasi->paket->harga * $reservasi->pax;
                @endphp
                <tr>
                    <td>{{ $reservasi->paket->nama_paket }}</td>
                    <td>Rp.{{ number_format($reservasi->paket->harga) }}</td>
                    <td>{{ $reservasi->pax }}</td>
                    <td>Rp. {{ number_format($totalPax) }}</td>
                </tr>
            </tbody>
        </table><br>
        <center>Item List</center><br>
        <table border="1" class="table" style="width:100%">
            <tr>
                <td>Product</td>
                <td>Price</td>
                <td>QTY</td>
                <td>Total</td>
            </tr>
            <tbody>
                @foreach($reservasi->detail_reservasi as $detail)
                    @php 
                        $total = $detail->barang->pembiayaan * $detail->qty;
                    @endphp
                    <tr>
                        @if($detail->is_include_paket == 1)
                            <td>{{ $detail->barang->nama_barang }} (Include dalam Paket)</td>
                            <td>Rp.0</td>
                            <td>{{ $detail->qty }}</td>
                            <td>0</td>
                        @else
                            <td>{{ $detail->barang->nama_barang }}</td>
                            <td>Rp.{{ number_format($detail->barang->pembiayaan) }}</td>
                            <td>{{ $detail->qty }}</td>
                            <td>{{ number_format($total) }}</td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
            <tbody>
                @php
                    $arr = 0;
                    foreach ($reservasi->detail_reservasi as $value) {
                        if($value->is_include_paket == 0){
                            $arr += $value->qty * $value->barang->pembiayaan;
                        }else{
                            $arr = 0;
                        }
                    }
                    $subtotal = $arr + $totalPax;
                @endphp
                <tr>
                    <td colspan="3" class="text-right">Sub Total</td>
                    <td>Rp. {{ number_format($subtotal) }}</td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">PPN</td>
                    <td>Rp. 0</td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">Grand Total</td>
                    <td>Rp. Rp. {{ number_format($subtotal) }}</td>
                </tr>
            </tbody>
        </table><br><br>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-7">
                    <!--<table>-->
                    <!--    <tr>-->
                    <!--        <td style="text-decoration: underline"><center>Customer</center></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td>(........................................................)</td>-->
                    <!--    </tr>-->
                    </table>
                </div>
                <div class="col-md-3">
                    <table class="text-right">
                        <tr>
                            <td style="text-decoration: underline"><center>Finance / Accounting</center></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td>(........................................................)</td>
                        </tr>
                        <tr>
                            <td><center>Signed</center></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-print">
        <div class="col-12">
            <button class="btn btn-primary btn-xl .no-print"  onclick="window.print();return false;"><i class="fas fa-print"></i>&nbsp; Print Quotation</button>
        </div>
    </div>
    <style>
        #invoice{
        padding: 30px;
    }
    
    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }
    
    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }
    
    .invoice .company-details {
        text-align: right
    }
    
    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }
    
    .invoice .contacts {
        margin-bottom: 20px
    }
    
    .invoice .invoice-to {
        text-align: left
    }
    
    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }
    
    .invoice .invoice-details {
        text-align: right
    }
    
    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }
    
    .invoice main {
        padding-bottom: 50px
    }
    
    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }
    
    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }
    
    .invoice main .notices .notice {
        font-size: 1.2em
    }
    
    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }
    
    .invoice table td,.invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }
    
    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }
    
    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }
    
    .invoice table .qty,.invoice table .total,.invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }
    
    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }
    
    .invoice table .unit {
        background: #ddd
    }
    
    .invoice table .total {
        background: #3989c6;
        color: #fff
    }
    
    .invoice table tbody tr:last-child td {
        border: none
    }
    
    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }
    
    .invoice table tfoot tr:first-child td {
        border-top: none
    }
    
    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }
    
    .invoice table tfoot tr td:first-child {
        border: none
    }
    
    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }
    
    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }
    
        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }
    
        .invoice>div:last-child {
            page-break-before: always
        }
    }
    </style>
</body>
</html>