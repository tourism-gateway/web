@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        {{-- <button type="button" class="btn btn-primary add-bussiness"><i class="fa fa-plus"></i> ADD NEW BUSSINESS</button> --}}
        <a href="{{ url('admin/reservasi/create') }}" class="btn btn-primary add-bussiness text-right"><i class="fa fa-plus"></i> ADD NEW RESERVATION</a><hr>
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Checkin</th>
                    <th>Checkout</th>
                    <th>Paket</th>
                    <th>PAX</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection

@section('js')
<!-- moment -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
           
            ajax: {
                url: HelperService.apiUrl('/admin/reservasi/datatables'),
                "type": "get",
            },
            // columnDefs: [
            //     {
            //         "targets": [ 1 ],
            //         "visible": false,
            //         "searchable": false
            //     },
            // ]
            columns: [
                // {
                //     data: "DT_RowIndex",
                //     name: "DT_RowIndex",
                //     sortable: false,
                //     searchable: false,
                //     width: "10%"
                // },
                {
                    data: "id_customer",
                    name: "id_customer",
                    render: function(data, type, row) {
                        return row.user.nama;
                    }
                },
                {
                    data: "checkin",
                    name: "checkin",
                    render: function(data, type, row) {
                        return moment(row.checkin).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "checkout",
                    name: "checkout",
                    render: function(data, type, row) {
                        return moment(row.checkout).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "id_paket",
                    name: "id_paket",
                    render: function(data, type, row) {
                        return row.paket.nama_paket;
                    }
                },
                {
                    data: "berapa_malam",
                    name: "berapa_malam",
                    render: function(data, type, row) {
                        return row.berapa_malam;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        view_link = "<a class='btn btn-primary btn-sm' href='" + HelperService.url('/admin/reservasi/detail/' + row.id) + "'><i class='fa fa-eye'></i></a>";
                        view_inv = "<a class='btn btn-success btn-sm' href='" + HelperService.url('/admin/reservasi/view-invoice/' + row.id) + "'><i class='fa fa-file-alt'></i></a>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link + ' ' + view_inv;
                    }
                },
            ],
        });
    } );
</script>
@endsection