<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Highland | Invoice</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4><img src="https://highlandcamp.robust.web.id/frontend/images/logo-highland.png" alt="" width="50px">
                    <small class="float-right">Date: {{ date('d-M-y', strtotime($data->created_at)) }}</small>
                    </h4>
                </div>
            <!-- /.col -->
            </div><hr>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    From
                    <address>
                    <strong>{{$data->user->name}}</strong><br>
                    Email : {{ $data->user->email }}<br>
                    {{$data->user->address}}
                    </address>
                </div>
            <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    To
                    <address>
                    <strong>{{$data->name}}</strong><br>
                    Checkin : {{ date('d-M-y', strtotime($data->checkin)) }}<br>
                    checkout : {{ date('d-M-y', strtotime($data->checkout)) }}<br>
                    </address>
                </div>
            <!-- /.col -->
            @php
                $characters = '1234567890';
            @endphp
            <div class="col-sm-4 invoice-col">
                <b>Invoice #{{ $data->code_invoice }}</b><br>
                <br>
                {{-- <b>Order ID:</b> 4F3S8J<br>
                <b>Payment Due:</b> 2/22/2014<br>
                <b>Account:</b> 968-34567 --}}
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Qty / PAX</th>
                        <th>Item / paket</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($item))
                        @foreach ($item as $i)
                        @php
                            $total = $i->price * $i->qty;
                        @endphp
                            <tr>
                                <td>{{ $i->qty }}</td>
                                <td>{{ $i->produk_name }}</td>
                                <td>{{ $i->price }}</td>
                                <td>Rp. {{ number_format($total) }}</td>
                            </tr>
                        @endforeach
                        @php
                            if(!empty($total)){
                                $total = $total;
                            }else{
                                $total = 0;
                            }
                            $total1 = $data->get_paket->harga * $data->pax;
                            $subtotal = $total1 + $total;
                            $sisa = $subtotal - $data->dp;
                        @endphp
                    @endif
                    
                    <tr>
                        <td>{{ $data->pax }}</td>
                        <td>{{ $data->get_paket->nama }}</td>
                        <td>{{ $data->get_paket->harga }}</td>
                        <td>Rp. {{ number_format($total1) }}</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="3"  class="text-right">Subtotal : </td>
                        <td>Rp. {{ number_format($subtotal) }} </td>
                    </tr>
                    <tr>
                        <td colspan="3"  class="text-right">Dp : </td>
                        <td>Rp. {{ number_format($data->dp) }} </td>
                    </tr>
                    <tr>
                        <td colspan="3"  class="text-right">Sisa Pembayaran : </td>
                        <td>Rp. {{ number_format($sisa) }} </td>
                    </tr>
                </tbody>
                </table>
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
            <!-- accepted payments column -->
            <div class="col-6">
                
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
            <div class="col-12">
                <button type="button" onclick="window.print();return false;" class="btn btn-success float-right"><i class="fas fa-print"></i> Print
                    Print
                </button>
            </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
</body>
</html>
