<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- Datatables -->
<link rel="stylesheet" href="{{ asset('adminAssets/css/dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/select.dataTables.min.css') }}">

<script type="text/javascript" src="{{ asset('adminAssets/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/dataTables.select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('https://cdn.datatables.net/plug-ins/1.10.20/sorting/currency.js') }}"></script>

<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">

<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>



<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<!--ck editor -->
<script src="https://cdn.jsdelivr.net/npm/@ckeditor/ckeditor5-build-classic@22.0.0/build/ckeditor.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/css-loader/3.3.3/css-loader.css">
<!-- select2 -->
<link rel="stylesheet" href="{{ asset('adminAssets/css/select2.min.css') }}">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"></script>
{{-- <script type="text/javascript" src="{{ asset('adminAssets/js/select2.min.js') }}"></script> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">


<!-- loadash -->
<script type="text/javascript" src="{{ asset('adminAssets/js/lodash.js') }}"></script>

<!-- Input Mask -->
<script type="text/javascript" src="{{ asset('adminAssets/js/jquery.inputmask.bundle.js') }}"></script>

<!-- popper Js -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
{{-- <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script> --}}

<!-- noty js -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<!-- izitoast -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>

<!-- axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<!-- vanilla input mask -->
<script type="text/javascript" src="{{ asset('adminAssets/js/vanilla-masker.min.js') }}"></script>

<!-- moment -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>


<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

{{-- Jquery Countdown --}}
<script src="https://cdn.jsdelivr.net/combine/npm/select2@4.0.13,npm/jquery-countdown@2.2.0" type="text/javascript"></script>

{{-- Google Maps --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmap3/7.2.0/gmap3.min.js" type="text/javascript"></script>


<script src="{{ asset('adminAssets/js/main.js') }}"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>

<script>
  $(document).on("wheel", "input[type=number]", function (e) {
      $(this).blur();
  });
</script>

<!-- Loader active -->
<div id="loading-indikator" class="loader loader-default"></div>

@if (Session::has('success'))
<script>
  swal.fire({
      type: 'success',
      title: '{{ Session::get("success") }}',
      icon: "success",
  });
</script>
@endif

@if (Session::has('error'))
<script>
  swal.fire({
      type: 'error',
      title: '{{ Session::get("error") }}',
      icon: "error",
  });
</script>
@endif

<script>
  $.fn.dataTable.ext.errMode = 'none';
  $('.btn').addClass('btn-hover-shine');

  var hideAvatarDropdown, hideModal;
  $(document).ready(function(e) {
      // agar user profile bisa diclik
      $(".link-dropdown-avatar").click(function() {
          $('.dropdown-avatar').toggleClass('show')
          $('.dropdown-noty').removeClass('show')
          hideAvatarDropdown = true;
      })

      $(".link-dropdown-noty").click(function() {
          $('.dropdown-noty').toggleClass('show')
          $('.dropdown-avatar').removeClass('show')
          hideAvatarDropdown = true;
      })

      $(document).click(function() {
          if (hideAvatarDropdown) {
              $('.dropdown-avatar').removeClass('show')
              $('.dropdown-noty').removeClass('show')
              hideAvatarDropdown = false;
          }
      });

      // $('.card').click(function(){
      //     if (hideModal) {
      //         $('.modal').modal('hide');
      //         hideModal = false;
      //     }
      // });

      // $('.modal').on('shown.bs.modal', function (e) {
      //     hideModal = true;
      // })
  });


  $.fn.select2.defaults.set("theme", "bootstrap");

  // Restricts input for the set of matched elements to the given inputFilter function.
  (function($) {
      $.fn.inputFilter = function(inputFilter) {
          return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
              if (inputFilter(this.value)) {
                  this.oldValue = this.value;
                  this.oldSelectionStart = this.selectionStart;
                  this.oldSelectionEnd = this.selectionEnd;
              } else if (this.hasOwnProperty("oldValue")) {
                  this.value = this.oldValue;
                  this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              } else {
                  this.value = "";
              }
          });
      };
  }(jQuery));

  // var local = window.location.hostname;
  var Helper = {
      onlyNumberInput: function(selector) {
          $(selector).inputFilter(function(value) {
              return /^\d*$/.test(value); // Allow digits only, using a RegExp
          });

          return this;
      },
      wysiwygEditor: function(selector) {
          ClassicEditor
              .create(document.querySelector(selector), {
                  allowedContent:true,
                  removePlugins: ['Heading', 'Link'],
                  toolbar: ['bold', 'italic', 'underline', 'bulletedList']
              })
              .then(editor => {
                  console.log(editor);
              })
              .catch(error => {
                  console.error(error);
              });
      },
      hideSidebar: function() {
          $('.hamburger--elastic').addClass('is-active');
          $('.fixed-sidebar').addClass('closed-sidebar');
      },
      ratingStar: function(star, light = '', dark = '') {
          if (light == '') {
              light = '<span class="star"><i class="fa fa-star"></i></span>';
          }
          if (dark == '') {
              dark = '<span class="star"><i class="fa fa-star-o"></i></span>';
          }

          rating = parseInt(star);
          starTemplate = '';

          if (rating > 0) {
              var range = _.range(0, rating);
              _.each(range, function(value, key) {
                  starTemplate += light + ' ';
              });

              if (rating < 5) {
                  var range = _.range(0, 5 - rating);
                  _.each(range, function(value, key) {
                      starTemplate += dark + ' ';
                  });
              }
          } else {
              var range = _.range(0, 5);
              _.each(range, function(value, key) {
                  starTemplate += dark + ' ';
              });
          }

          return starTemplate;
      },
      // url: function(url = '') {
      //     return 'http://'+local+'/' + url;
      // },
      // apiUrl: function(url = '') {
      //     return 'http://'+local+'/api' + url;
      // },
      thousandsSeparators: function(x) {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      },
      loadingStart: function() {
          $('#loading-indikator').addClass('is-active');
      },
      loadingStop: function() {
          $('#loading-indikator').removeClass('is-active');
      },

      url: function(url = '') {
          return '{{ url("/") }}' + url;
      },

      apiUrl: function(url = '') {
          return '{{ url("/") }}/api' + url;
      },

      redirectUrl: function(url) {
          return '{{ url("/") }}' + url;
      },

      frontApiUrl: function(url) {
          return '{{ url("/") }}/admin' + url;
      },

      saveToLocalstorage: function(key, val) {
          // val = JSON.stringify(val)
          localStorage.setItem(key, val);
      },

      numberRow: function(table) {
          table.on('order.dt search.dt', function() {
              table.column(0, {
                  search: 'applied',
                  order: 'applied'
              }).nodes().each(function(cell, i) {
                  cell.innerHTML = i + 1;
              });
          }).draw();
      },

      infoNotif: function(text) {
          this.loadingStop();

          toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": true,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "100000",
              "hideDuration": "100000",
              "timeOut": "100000",
              "extendedTimeOut": "100000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }

          toastr.info(text)

          return this;
      },

      successNotif: function(text) {
          this.loadingStop();

          toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": true,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "100000",
              "hideDuration": "100000",
              "timeOut": "100000",
              "extendedTimeOut": "100000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }

          toastr.success(text)

          return this;
      },

      errorNotif: function(text) {
          toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": true,
              "positionClass": "toast-top-right",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "100000",
              "hideDuration": "100000",
              "timeOut": "100000",
              "extendedTimeOut": "100000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }

          toastr.error(text)

          return this;
      },

      warningNotif: function(text) {
          toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": true,
              "positionClass": "toast-top-right",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "100000",
              "hideDuration": "100000",
              "timeOut": "100000",
              "extendedTimeOut": "100000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }

          toastr.warning(text)

          return this;
      },

      errorMsgRequest: function(xhr, status, error) {
          this.errorNotif(xhr.responseJSON.msg);
          console.log('xhr', xhr)
          console.log('status', status)
          console.log('error', error)

          if (xhr.status == 422) {
              error = xhr.responseJSON.data;
              _.each(error, function(pesan, field) {
                  // send notif
                  // Helper.errorNotif(pesan[0]);
                  // clean msg
                  $('.error-validation-mgs').remove();
                  // append new message
                  // $('[name="' + field + '"]').after('<span class="error-validation-mgs" id="error-' + field + '" style="display:block; color:red"><br/>' + pesan[0] + '</span>');
                  // stop loop
                  return false;
              })
          }
      },

      errorsAlert: function(xhr, status, error, callback = null) {
          // alert('asd');
          console.log('xhr', xhr)
          console.log('status', status)
          console.log('error', error)

          if (xhr.status == 400) {
              error = xhr.responseJSON.data;
              if(callback == null){
                  this.errorNotif(xhr.responseJSON.msg);
                  $('.error-validation-mgs').remove();
                  $('#append_message').after('<div class="alert alert-danger" id="alert-" role="alert">"'+ pesan[0] +'"</div>');
              }else{
                  callback(xhr.responseJSON.msg)
              }
          }
      },

      toSlug: function(str, elem) {
          //replace all special characters | symbols with a space
          str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();
          // trim spaces at start and end of string
          str = str.replace(/^\s+|\s+$/gm, '');
          // replace space with dash/hyphen
          str = str.replace(/\s+/g, '-');
          $('#' + elem + '').val(str);
          //return str;
      },

      // create confirm dialog
      confirm: function(callbackAction, option = null) {
          if (option == null) {
              option = {
                  title: "Are You Sure?",
                  message: "You sure about this?",
              };
          }
          bootbox.confirm({
              title: "Are You Sure?",
              message: option.message,
              className: 'bootbox-my-modal-style',
              buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
              callback: function(result) {
                  if (result) {
                      callbackAction();
                  }
              }
          });
      },

      confirmDelete: function(callbackAction) {
          Helper.confirm(callbackAction, {
              title: "DeLete Data",
              message: "Do you want to delete this data?",
          })
      },

      deleteMsg: function() {
          return 'Data successfully deleted';
      },

      // redirect
      redirectTo: function(url = '') {
          if (url == '') {
              location.reload();
              return;
          }
          window.location.href = Helper.redirectUrl(url);
      },

      // axios handle error
      handleErrorResponse: function(error) {
          console.log('handleErrorResponse', error);
          this.loadingStop();
          if (error.response) {
              // console.log(error.response.data);
              // console.log(error.response.status);
              // console.log(error.response.headers);

              if (error.response.status == 422) {

                  messages = error.response.data.data;
                  // clean msg
                  $('.error-validation-mgs').remove();
                  _.each(messages, function(pesan, field) {
                      // append new message
                      $('[name="' + field + '"]').after('<strong><span class="error-validation-mgs" id="error-' + field + '" style="color:red; display:block;">' + pesan[0] + '</span></strong>');
                      // $('[name="' + field + '"]').after('<em class="error invalid-feedback" id="' + field + '-error">' + pesan[0] + '</em>');
                      // stop loop
                      return false;
                  })

              }
              this.errorNotif(error.response.data.msg);
          }

          return this;
      },

      serializeForm: function($el) {
          data = {};
          $.each($el.serializeArray(), function(i, field) {
              if (_.includes(field.name, '[]') !== true) {
                  data[field.name] = field.value;
              } else {
                  field_name = field.name.replace('[]', '');
                  data[field_name] = $('[name="' + field.name + '"]').map(function() {
                      return $(this).val()
                  }).get();
              }
          });

          return data;
      },
      currency: function(el) {
          VMasker($('' + el + '')).maskMoney({
              precision: 0,
              separator: ',',
              delimiter: '.',
              unit: 'Rp.',
              zeroCents: false,

          });
      },

      thousandSeparatorMaskInput: function(el) {
          VMasker($('' + el + '')).maskMoney({
              precision: 0,
              separator: ',',
              delimiter: '.',
              zeroCents: false,
          });
      },

      toCurrency: function(val) {
          return new Intl.NumberFormat('de-DE').format(val);

      },

      unMask: function(el) {
          var el = $('' + el + '')
          return VMasker(el).unMask();
      },

      date: function(el, start, format = 'Y-m-d', func) {
          var date = new Date();
          date.setDate(date.getDate() + start);
          $('' + el + '').datetimepicker({
              minDate: date,
              format: 'Y-m-d',
              formatDate: 'Y-m-d',
              timepicker: false,
              numberOfMonths: 3,
              // inline:true,
          });
      },
      dateFormat: function (el) {
          $('' + el + '').datetimepicker({
              timepicker:false,
              format:'d-m-Y',
          });
          $('' + el + '').keydown(function(e) {
              var elid = $(document.activeElement).is("input:focus");
              if(e.keyCode === 8 && !elid){
                  return false
              }
              return false;
          });
      },
      datePick: function(el) {
          $(el).datetimepicker({
              // format: 'm/d/Y',
              // formatDate: 'm/d/Y',
              format: 'Y-m-d',
              formatDate: 'Y-m-d',
              timepicker: false,
              timepickerScrollbar: false,
              scrollMonth: false,
              scrollTime: false,
              scrollInput: false,
              numberOfMonths: 3,
          });
      },

      removeArrayValue: function(arr, item){
          position = arr.indexOf(item);
          if (~position) arr.splice(position, 1);
      },

      noNegative: function() {
          $(':input[type="number"]').keypress(function(event) {
              if (event.which == 45 || event.which == 189) {
                  event.preventDefault();
              }
          });
      }
  }

  var DataTablesHelper = {
      table: null,
      config: {
          prefixName: '',
          modalSelector: null,
          modalButtonSelector: null,
          modalFormSelector: null,
          upload: false,
          url: '',
          export: false,
          column_export: [],
          orderBy: [],
          selector: '#table',
          columns: [],
          columnDefs: [],
          columnsField: [],
          actionLink: {
              only_icon: false,
              render: null,
              store: '',
              update: '',
              delete: '',
              detail: '',
              add_detail: '',
              export_detail_excel: '',
              deleteBatch: '',
              mutation: ''
          },
          dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
              "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-6'i><'col-sm-6'p>>",
          selectable: false,

          // buttonDeleteBatch: '.delete-selected',
          // buttonImportExcel: '#import',
          // modalImportExcel: '#importExcel'

      },
      reloadTable: function(url = null) {
          if (url == null) {
              this.table.ajax.reload();
          } else {
              this.table.ajax.url(url).load();
          }

          return this;
      },
      configuration: function(url, columns, columnDefs) {
          var buttons =[];
          if(DataTablesHelper.config.export == true) {
              var column_export = ':visible';
              if(DataTablesHelper.config.column_export.length) {
                  var column_export = DataTablesHelper.config.column_export
              }
               buttons = [{
                  extend: 'excelHtml5',
                  text: 'Export Excel',
                  exportOptions: {
                      columns: column_export
                  }
              }]
          }else{
              buttons = [{
                      extend: 'colvis'
                  },
                  {
                      text: '<i class="fas fa-sync"></i>',
                      action: function(e, dt, node, config) {
                          dt.ajax.reload();
                      }
                  }
              ];
          }

          if (DataTablesHelper.config.selectable) {
              return {
                  processing: true,
                  serverSide: true,
                  select: {
                      style: 'single',
                  },

                  columnDefs: [{
                          orderable: false,
                          searchable: false,
                          defaultContent: '',
                          className: 'select-checkbox text-left',
                          targets: 0,
                          sortable: false,
                      },
                      {
                          targets: 1,
                          className: 'text-center',
                          searchable: false,
                          sortable: false
                          // render: $.fn.dataTable.render.number('.', ',', 2 )
                      },
                      {
                          type: 'html',
                          targets: '_all',
                      }
                  ],
                  dom: DataTablesHelper.config.dom,
                  buttons: buttons,
                  ordering: 'true',
                  order: DataTablesHelper.config.orderBy,
                  // responsive: true,
                  language: {
                      buttons: {
                          colvis: '<i class="fa fa-list-ul"></i>'
                      },
                      search: '',
                      searchPlaceholder: "Search...",
                      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                  },
                  oLanguage: {
                      sLengthMenu: "_MENU_",
                  },
                  ajax: {
                      url: Helper.apiUrl(url),
                      "type": "get",
                      "data": function(d) {
                          return $.extend({}, d, {
                              "extra_search": $('#extra').val()
                          });
                      }
                  },
                  columns: columns
              }
          } else {
              return {
                  processing: true,
                  serverSide: true,
                  ordering: true,
                  order: DataTablesHelper.config.orderBy,
                  columnDefs: columnDefs,
                  language: {
                      buttons: {
                          colvis: '<i class="fa fa-list-ul"></i>'
                      },
                      search: '',
                      searchPlaceholder: "Search...",
                      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                  },
                  oLanguage: {
                      sLengthMenu: "_MENU_",
                  },
                  dom: DataTablesHelper.config.dom,
                  buttons: buttons,
                  ajax: {
                      url: Helper.apiUrl(url),
                      "type": "get",
                      "data": function(d) {
                          return $.extend({}, d, {
                              "extra_search": $('#extra').val()
                          });
                      }
                  },
                  columns: columns
              }
          }
      },
      make: function(selector, url, columns, columnDefs) {
          return $(selector).DataTable(this.configuration(url, columns, columnDefs));
      },
      generateColumn: function(columnsField, config) {
          columns = [];
          _.each(columnsField, function(val, key) {
          if (typeof val === 'string') {
              columns.push({
                  data: val,
                  name: val,
                  render: function(data, type, full) {
                      if(data == null) {
                          return '-';
                      } else {
                          return data;
                      }
                  }
              })
          } else {
              columns.push(val)
          }


          });

          prefixClass = config.selector.replace('#', '');
          if(this.config.actionLink !== '') {
              columns.push({
                  data: 'id',
                  name: 'id',
                  searchable: false,
                  orderable: false,
                  render: function(data, type, full) {
                      var add_detail_btn ='';
                      var edit_btn ='';
                      var delete_btn ='';
                      var detail_btn ='';
                      var export_detail_btn ='';

                      add_detail_btn = "<button title='Add Detail' data-id='" + full.id + "' class='" + prefixClass + "-add_detail btn-hover-shine btn btn-primary btn-sm btn-white'><i class='fa fa-plus'></i> "+(this.config.actionLink.only_icon === true ? "Add Detail" : "")+"</button>";

                      edit_btn = "<button title='Edit' data-id='" + full.id + "' class='" + prefixClass + "-edit btn-hover-shine btn btn-warning btn-sm btn-white'><i class='fa fa-pencil'></i> "+(this.config.actionLink.only_icon === true ? "EDIT" : "")+"</button>";

                      delete_btn = "<button title='Delete' data-id='" + full.id + "'  class='" + prefixClass + "-delete btn-hover-shine btn btn-danger btn-sm'><i class='fa fa-trash'></i> "+(this.config.actionLink.only_icon === true ? "DELETE" : "")+"</button>";

                      detail_btn = " <button title='Detail' data-id='" + full.id + "'  class='" + prefixClass + "-detail btn-hover-shine btn btn-secondary btn-sm'><i class='fa fa-bars'></i> "+(this.config.actionLink.only_icon === true ? "DETAIL" : "")+"</button>";

                      export_detail_btn = " <button title='Export Detail' data-id='" + full.id + "'  class='" + prefixClass + "-export_detail_excel btn-hover-shine btn btn-success btn-sm'><i class='fa fa-file-excel-o'></i> "+(this.config.actionLink.only_icon === true ? "Export Detail Excel" : "")+"</button>";

                      if (this.config.actionLink.render != null) {
                          actionData = {
                              edit_btn,
                              delete_btn,
                          }
                          return config.actionLink.render(data, type, full, actionData);
                      }
                      return edit_btn + " " + delete_btn + (this.config.actionLink.detail != null ? detail_btn : "") + (this.config.actionLink.export_detail_excel != null ? export_detail_btn : "") + " " + (this.config.actionLink.add_detail != null ? add_detail_btn : "");
                  }
              });
          }

          return columns;
      },
      setConfig: function(configuration) {
          new_config = this.config;
          _.each(configuration, function(val, key) {
              new_config[key] = val;
          })
          this.config = new_config;

          return new_config;
      },
      generate: function(configuration) {
          config = this.setConfig(configuration);

          // hide btn delete batch
          if (config.selectable) {
              $(config.buttonDeleteBatch).hide();
          }

          if (config.columns.length) {
              this.table = this.make(
                  config.selector, config.url, config.columns, config.columnDefs
              );
          }

          if (config.columnsField.length) {
              this.table = this.make(
                  config.selector, config.url, this.generateColumn(config.columnsField, config), config.columnDefs
              );

              // event here
              this.datatablesEvent();
          }

          return this;
      },
      datatablesEvent: function() {
          config = this.config;

          prefixClass = config.selector.replace('#', '');
          this.prefixName = prefixClass;

          // show btn delete batch
          if (config.selectable) {
              DataTablesHelper.table.on('deselect', function(e, dt, type, indexes) {
                  if (type === 'row') {
                      var data = DataTablesHelper.table.rows('.selected').data();
                      console.log('deselect', data.length)
                      if (data.length) {
                          $(config.buttonDeleteBatch).show();
                      } else {
                          $(config.buttonDeleteBatch).hide();
                      }
                  }
              });

              DataTablesHelper.table.on('select', function(e, dt, type, indexes) {
                  if (type === 'row') {
                      var data = DataTablesHelper.table.rows('.selected').data();
                      console.log('data.length', data.length)
                      if (data.length) {
                          $(config.buttonDeleteBatch).show();
                      } else {
                          $(config.buttonDeleteBatch).hide();
                      }
                  }
              });
          }

          // edit data
          $(document)
              .on('click', "." + prefixClass + "-edit", function() {
                  var row = DataTablesHelper.table.row($(this).parents('tr')).data();

                  if (config.modalSelector !== null) {
                      ModalHelper.modalShow('Edit ' + prefixClass, row);
                  } else {
                      Helper.redirectTo(config.actionLink.update(row));
                  }

              })

           // detail data
           $(document)
              .on('click', "." + prefixClass + "-detail", function() {
                  var row = DataTablesHelper.table.row($(this).parents('tr')).data();

                  // if (config.modalSelector !== null) {
                  //     ModalHelper.modalShow('Detail ' + prefixClass, row);
                  // } else {
                      Helper.redirectTo(config.actionLink.detail(row));
                  // }

              })

          // add detail data
           $(document)
              .on('click', "." + prefixClass + "-add_detail", function() {
                  var row = DataTablesHelper.table.row($(this).parents('tr')).data();

                  if (config.modalSelector !== null) {
                      ModalHelper.modalShow('Add Detail ' + prefixClass, row);
                  } else {
                      Helper.redirectTo(config.actionLink.add_detail(row));
                  }

              })

          // export detail excel
           $(document)
              .on('click', "." + prefixClass + "-export_detail_excel", function() {
                  var row = DataTablesHelper.table.row($(this).parents('tr')).data();

                  if (config.modalSelector !== null) {
                      ModalHelper.modalShow('Export Detail Excel ' + prefixClass, row);
                  } else {
                      Helper.redirectTo(config.actionLink.export_detail_excel(row));
                  }

              })

          // delete data
          $(document).on('click', "." + prefixClass + "-delete", function() {
              var row = DataTablesHelper.table.row($(this).parents('tr')).data();
              globalCRUD.delete(config.actionLink.delete(row))
          })
          // jika selected row treu
          if (config.selectable) {
              // console.log(config.buttonDeleteBatch);
              $(document).on('click', config.buttonDeleteBatch, function() {
                  var ids = $.map(DataTablesHelper.table.rows('.selected').data(), function(item) {
                      return item.id;
                  });
                  console.log(ids);
                  globalCRUD.delete(config.actionLink.deleteBatch(ids), {
                      id: ids
                  });
                  console.log(ids)
              });
          }
          if (config.import) {
              $(document).on('click', config.buttonImportExcel, function() {
                  var fd = new FormData();
                  var files = $('#file')[0].files[0];
                  if (files) {
                      fd.append('file', files);
                  }
                  globalCRUD.import(config.actionLink.import(), fd);
                  setTimeout(function() {
                      $(config.modalImportExcel).hide();
                  }, 1500);
              });
          }
      },

  }

  var ModalHelper = {
      modul: '',
      modal: null,
      button: null,
      form: null,
      upload: null,
      config: {},
      generate: function(configuration) {
          this.config = configuration;
          this.modal = $(configuration.modalSelector).modal({
              show: false
          });
          this.button = configuration.modalButtonSelector;
          this.modul = configuration.prefixName;
          this.form = configuration.modalFormSelector;
          this.upload = configuration.upload;
          if (configuration.modalFormSelector !== null) {
              this.modalEvent();
          }

          return this;
      },
      modalEvent: function(configuration) {
          // add data
          var is_upload = this.upload;
          $(document).on('click', this.button, function() {
              textModal = 'Add ' + ModalHelper.modul;
              ModalHelper.modalShow(textModal.toUpperCase());
          });

          // submit data
          $(document).on('submit', this.form, function(e) {
              var data = null;
              if(is_upload == true) {
                  data = new FormData(this);
              } else {
                  data = Helper.serializeForm($(ModalHelper.form));
              }
              var d = {};
              var id = '';
              if($('#id').val() != null) {
                  id = $('#id').val();
              }
              if(data.id != null) {
                  id = data.id
              }

              d.id = id;
              if (d.id == '') {
                  globalCRUD.store(ModalHelper.config.actionLink.store(), data);
              } else {
                  globalCRUD.update(ModalHelper.config.actionLink.update(d), data, is_upload);
              }
              e.preventDefault();
          });
      },
      modalShow: function(text, row = null) {
          // clean msg
          $('.error-validation-mgs').remove();

          this.modal.modal('show');
          if (row == null) {
              this.kosongkanDataFormModal();
          } else {
              this.isiDataFormModal(row)
          }
      },
      modalClose: function() {
          this.modal.modal('hide');
      },
      kosongkanDataFormModal: function() {
          row = Helper.serializeForm($(this.form));
          _.each(row, function(val, key) {
              $('[name="' + key + '"]').val('').change();
          })
          $('[name="file"]').val(null);
          $('#img-upload').attr('src', 'http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png');
          $('#upload-file-info').html('');
      },
      isiDataFormModal: function(row) {
          $('#upload-file-info').html('');
          if(this.upload == true) {
              if(row.images != null) {
                  // $('#img-upload').attr('src', Helper.url(row.link_image+row.images))
                  $('#img-upload').attr('src', Helper.url(row.link_image));
              } else {
                  $('#img-upload').attr('src', 'http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png')
              }
          }
          _.each(row, function(val, key) {
              if (key == 'product_part_category_id') {
                  console.log(key, val)
              }
              $('[name="' + key + '"]').val(val).change();
          })
      }
  };

  var globalCRUD = {
      requestAjax: null,
      redirect: '/',
      successHandle: null,
      errorHandle: null,
      table: null,
      form: null,
      modal: null,
      datatables: function(configuration) {
          table = DataTablesHelper.generate(configuration);
          this.table = table;

          if (table.config.modalSelector !== null) {
              this.modal = ModalHelper.generate(DataTablesHelper.config);
          }

          return this;
      },
      show: function(url) {
          return Axios.get(url);
      },
      store: function(url, data) {
          Helper.loadingStart();
          store = Axios.post(url, data);
          this.requestAjax = store;
          store.then(function(response) {
                  console.log(response);
                  if (globalCRUD.successHandle !== null) {
                      globalCRUD.successHandle(response);
                  } else {
                      globalCRUD.getDefaultSuccessHandle(response);
                  }
              })
              .catch(function(error) {
                  if (globalCRUD.errorHandle !== null) {
                      globalCRUD.errorHandle(error);
                  } else {
                      globalCRUD.getDefaultErrorHandle(error);
                  }
              });
      },
      storeTo: function(url, data = null) {
          if (data == null) {
              data = Helper.serializeForm(this.form);
          }

          if (typeof data === 'function') {
              data = data(Helper.serializeForm(this.form));
          }

          if (typeof url === 'function') {
              this.store(url, data);
          } else {
              this.store(url, data);
          }

          return this;
      },
      update: function(url, data, is_upload = false) {
          Helper.loadingStart();
          var update = null;
          if(is_upload == true) {
              update = Axios.post(url, data);
          } else {
              update = Axios.put(url, data);
          }
          this.requestAjax = update;

          update.then(function(response) {
                  if (globalCRUD.successHandle !== null) {
                      globalCRUD.successHandle(response);
                  } else {
                      globalCRUD.getDefaultSuccessHandle(response);
                  }
              })
              .catch(function(error) {
                  if (globalCRUD.errorHandle !== null) {
                      globalCRUD.errorHandle(error);
                  } else {
                      globalCRUD.getDefaultErrorHandle(error);
                  }
              });
      },
      updateTo: function(url, data = null) {
          if (data == null) {
              data = Helper.serializeForm(this.form);
          }

          if (typeof data === 'function') {
              data = data(Helper.serializeForm(this.form));
          }

          if (typeof url === 'function') {
              this.update(url(data), data);
          } else {
              this.update(url, data);
          }

          return this;
      },
      delete: function(url, data = null) {
          Helper.confirmDelete(function() {
              Helper.loadingStart();
              Axios.delete(url, {
                      data: data
                  }).then(function(response) {
                      globalCRUD.axiosResponseSuccess(response);
                  })
                  .catch(function(error) {
                      globalCRUD.axiosResponseError(error);
                  })
          })
      },
      import: function(url, data = null) {
          Axios.post(url, data).then(function(response) {
              globalCRUD.axiosResponseSuccess(response);
              console.log('response', response);
          }).catch(function(error) {
              globalCRUD.axiosResponseError(error);
              console.log('error', error);
          })
      },
      handleSubmit: function($el) {
          this.form = $el;
          return this;
      },
      getDefaultSuccessHandle: function(response) {
          Helper.loadingStop();

          if (response.status == 204) {
              // send notif
              Helper.successNotif(Helper.deleteMsg());
          } else {
              // send notif
              Helper.successNotif(response.data.msg);
          }

          if (this.table !== null) {
              this.table.reloadTable();
          }

          if (table.config.modalSelector !== null) {
              ModalHelper.modalClose();
          }

          if (this.redirect !== '/') {
              Helper.redirectTo(this.redirect());
          }
      },
      getDefaultErrorHandle: function(error) {
          Helper.loadingStop();
          Helper.handleErrorResponse(error)
      },
      axiosResponseSuccess: function(response) {
          if (globalCRUD.successHandle !== null) {
              globalCRUD.successHandle(response);
          } else {
              globalCRUD.getDefaultSuccessHandle(response);
          }
      },
      axiosResponseError: function(error) {
          if (globalCRUD.errorHandle !== null) {
              globalCRUD.errorHandle(error);
          } else {
              globalCRUD.getDefaultErrorHandle(error);
          }
      },
      redirectTo: function(url, full_url = false) {
          if (this.requestAjax !== null) {
              this.requestAjax.then(function(response) {
                  if (typeof url === 'function') {
                      if (full_url) {
                          window.location.href = url(response);
                      } else {
                          Helper.redirectTo(url(response));
                      }
                  } else {
                      Helper.redirectTo(url);
                  }
              })
          }

          if (this.requestAjax == null) {
              Helper.redirectTo(url);
          }
      },
      backTo: function(url) {
          this.redirectTo(url, true);
      },
      select2Static: function(selector, url, callback = null) {
          Axios.get(url)
              .then(function(response) {
                  data = response.data.data;

                  if (callback !== null) {
                      res = $.map(data, callback);
                  } else {
                      res = $.map(data, function(item) {
                          return {
                              text: item.name,
                              id: item.id
                          }
                      });
                  }

                  $(selector).select2({
                      data: res
                  });
              })
              .catch(function(error) {
                  Helper.handleErrorResponse(error)
              });

          return this;
      },

      select2: function(selector, url = null, callback = null, visible = true) {
          if (url == null) {
              $(selector).select2();
              return this;
          }

          $(selector).select2({
              tags: false,
              ajax: {
                  type: "GET",
                  url: Helper.apiUrl(url),

                  data: function(params) {
                      return {
                          q: params.term
                      };
                  },
                  processResults: function(data) {
                      var res = $.map(data.data, function(item) {
                          return {
                              text: item.name,
                              id: item.id
                          }
                      });

                      if (callback !== null) {
                          var res = $.map(data.data, callback);
                      }
                      return {
                          results: res
                      };
                  }
              },
              closeOnSelect: true,

              containerCss: function (element) {
                  var style = $(element)[0].style;
                  return {
                      display: visible == false ? style.display : style.display.inline
                  };
              }

          });
          return this;
      },

      select2Tags: function(selector, url = null, callback = null) {
          if (url == null) {
              $(selector).select2({
                  tags: true
              });
              return this;
          }

          $(selector).select2({
              ajax: {
                  type: "GET",
                  url: Helper.apiUrl(url),
                  data: function(params) {
                      return {
                          q: params.term
                      };
                  },
                  processResults: function(data) {
                      var res = $.map(data.data, function(item) {
                          return {
                              text: item.name,
                              id: item.id
                          }
                      });

                      if (callback !== null) {
                          var res = $.map(data.data, callback);
                      }
                      return {
                          results: res
                      };
                  }
              },


              tags: true,


          });
          return this;
      }
  }


  // axios instance
  const Axios = axios.create({
      baseURL: Helper.apiUrl(),
      timeout: 100000,
      headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // jquery ajax
  $.ajaxPrefilter(function(options) {
      options.beforeSend = function(xhr) {
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
          xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
      }
  });
  Helper.noNegative();
</script>

<!-- <script src="{{ asset('js/echo.js') }}"></script>
<script src="https://js.pusher.com/6.0/pusher.min.js"></script> -->
<script>
  const animateCSS = (element, animation, prefix = 'animate__') =>
      // We create a Promise and return it
      new Promise((resolve, reject) => {
          const animationName = `${prefix}${animation}`;
          const node = document.querySelector(element);

          node.classList.add(`${prefix}animated`, animationName);

          // When the animation ends, we clean the classes and resolve the Promise
          function handleAnimationEnd() {
              node.classList.remove(`${prefix}animated`, animationName);
              node.removeEventListener('animationend', handleAnimationEnd);

              resolve('Animation ended');
          }

          node.addEventListener('animationend', handleAnimationEnd);
      });

  // window.Echo = new Echo({
  //     broadcaster: 'pusher',
  //     key: 'a8394f76d66c65c1a5c6',
  //     cluster: 'ap1',
  //     headers: {
  //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //     }
  // });

  // Echo.private('chats.reply.{{ Auth::id() }}')
  //     .listen('.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', (e) => {
  //         animateCSS('.link-dropdown-noty', 'bounce').then((message) => {
  //             $('.link-dropdown-noty').removeClass('animate__animated animate__bounce');
  //         });

  //         var audio = new Audio('{{  asset("/storage/order_problem/" . "brah.mp3") }}');
  //         audio.play();

  //         $('.count-notif').text(e.content.count_unread);
  //         console.log('pusher echo', e)
  //         // Helper.infoNotif(e.name + '  ' + e.description);
  //         if ($('#content').length) {
  //             // $('#chats').html(e.content.list);
  //             $('#chat-content[data-chat="' + e.content.id + '"]').append(e.content.reply);
  //             $("#content").animate({
  //                 scrollTop: $('#content')[0].scrollHeight + 100
  //             }, 1000);
  //         }
  //     });
</script>