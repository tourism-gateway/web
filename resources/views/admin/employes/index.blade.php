@extends($master_template)

@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-employes-btn"><i class="fa fa-plus"></i> ADD NEW USER</button>
        <table id="employess_tables"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


<form id="employe-create-form">
    <div id="employe-create-modal" class="modal fade" tabindex="-1" aria-labelledby="employe-create-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="employe-create-modalLabel">ADD NEW USER
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label>Users</label>
                        <select class="form-control col-12" name="user_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($user as $usr)
                                <option value="{{ $usr->name }}">{{ $usr->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Bussiness</label>
                        <select class="form-control col-12" name="bussiness_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($bussiness as $item)
                                <option value="{{ $item->nama_bisnis }}">{{ $item->nama_bisnis }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Phone</label>
                        <input type="number" name="phone" class="form-control" value="" placeholder="enter Phone" >
                    </div>
                    <div class="mb-3">
                        <label>Divisi</label>
                        <input type="text" name="divisi" class="form-control" value="" placeholder="enter Divisi" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="employes-edit-form">
    <div id="employes-edit-modal" class="modal fade" tabindex="-1" aria-labelledby="employes-edit-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="employes-edit-modalLabel">EDIT EMPLOYES
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <input type="hidden" name="id" />
                        <label>Users</label>
                        <select class="form-control col-12" name="user_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($user as $usr)
                                <option value="{{ $usr->name }}">{{ $usr->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Bussiness</label>
                        <select class="form-control col-12" name="bussiness_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($bussiness as $item)
                                <option value="{{ $item->nama_bisnis }}">{{ $item->nama_bisnis }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Phone</label>
                        <input type="number" name="phone" class="form-control" value="" placeholder="enter Phone" >
                    </div>
                    <div class="mb-3">
                        <label>Divisi</label>
                        <input type="text" name="divisi" class="form-control" value="" placeholder="enter Divisi" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#employess_tables');
        var $modalCreate = $('#employe-create-modal');
        var $modalEdit = $('#employes-edit-modal');

        var $formCreate = new FormService($('#employe-create-form'));
        var $formEdit = new FormService($('#employes-edit-form'));

        var $http = new HttpService();

        var $httpCreate = new HttpService({
            formService: $formCreate,
            bootrapTable: $table,
            bootrapModal: $modalCreate,
        });

        var $httpEdit = new HttpService({
            formService: $formEdit,
            bootrapTable: $table,
            bootrapModal: $modalEdit,
        });

        $table.bootstrapTable({
            sortName: 'phone',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/employes',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Bussines Name',
                    field: 'bussiness_id',
                    sortable: true,
                },
                {
                    title: 'Users',
                    field: 'user_id',
                    sortable: true,
                },
                {
                    title: 'Phone',
                    field: 'phone',
                    sortable: true,
                },
                {
                    title: 'Divisi',
                    field: 'divisi',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $formEdit.emptyFormData();
                            $modalEdit.modal('show')
                            $formEdit.setFormData({
                                id: row.id,
                                phone: row.phone,
                                divisi: row.divisi,
                                user_id: row.user_id,
                                bussiness_id: row.bussiness_id,
                            });
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/employes/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-employes-btn').click(function() {
            $formCreate.emptyFormData();
            $modalCreate.modal('show')
        });

        $formCreate.onSubmit(function(data) {
            $httpCreate.post('/api/admin/employes', data);
        });

        $formEdit.onSubmit(function(data) {
            $httpEdit.put(`/api/admin/employes/${data.id}/update`, data)
        });
    })
</script>
@endsection