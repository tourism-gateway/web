<?php

namespace App\Service\User;

use App\Models\User;

class UserDeleteService
{
    public function deleteById($id)
    {
        return User::where('id', $id)->delete();
    }
}
