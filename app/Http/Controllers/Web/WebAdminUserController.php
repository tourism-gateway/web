<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Spatie\Permission\Models\Role;
use App\Models\Company;

class WebAdminUserController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.user.index', [
            'title' => 'users',
            'roles' => Role::all(),
            'company' => Company::all()
        ]);
    }
}
