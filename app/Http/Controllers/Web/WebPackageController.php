<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use App\Models\Bussiness;
use App\Models\ItemVendors;

class WebPackageController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.package.index', [
            'title' => 'package',
            'bussiness' => Bussiness::get(),
            'vendor_item' => ItemVendors::get(),
        ]);
    }
}
