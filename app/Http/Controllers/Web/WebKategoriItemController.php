<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class WebKategoriItemController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.kategory-item.index', [
            'title' => 'Kategory Item',
        ]);
    }
}
