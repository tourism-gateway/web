<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bussiness;

class WebPegawaiController extends WebController
{
    public function index()
    {
        $user = User::get();
        $bussiness = Bussiness::get();
        return $this->loadView('admin.employes.index', [
            'title' => 'Employes List',
            'user' => $user,
            'bussiness' => $bussiness,
        ]);
    }

    public function select2User()
    {
        
        return $this->successResponse($user, 'msg');
    }

    public function select2Bussiness()
    {
        
        return $this->successResponse($bussiness, 'msg');
    }
}
