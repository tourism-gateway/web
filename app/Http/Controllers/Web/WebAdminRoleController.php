<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class WebAdminRoleController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.role.index', [
            'title' => 'role',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.role.create', [
            'title' => 'create role',
            'permissions' => Permission::all()
        ]);
    }

    public function edit($id)
    {
        $role = Role::with('permissions')->where('id', $id)->firstOrFail();
        $permissions = Permission::all()->map(function ($row) use ($role) {
            $checked = $role->permissions->where('id', $row->id)->first() == null ? '' : 'checked';
            return [
                'id' => $row->id,
                'name' => $row->name,
                'checked' => $checked,
            ];
        });

        return $this->loadView('admin.role.edit', [
            'title' => 'role edit',
            'permissions' => $permissions,
            'role' => $role
        ]);
    }
}
