<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class WebBussinessController extends WebController
{
    public function index()
    {
        
        return $this->loadView('admin.bussiness.index', [
            'title' => 'Bussiness List'
        ]);
    }
}
