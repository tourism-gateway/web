<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class WebReservationgController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.web-setting.reservation.index', [
            'title' => 'Reservation List',
        ]);
    }
}
