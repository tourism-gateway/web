<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\ItemVendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Reservation;
use App\Models\User;
use App\Models\Customer;
use App\Models\HistoryItemService;
use App\Models\Package;
use App\Models\Reservasi;
use DB;
use Carbon\Carbon;

class ApiReservasiController extends ApiController
{
    public function index()
    {
        $list = Reservation::with(['user', 'paket'])->get();
        return $this->successResponse($list, 'Data Berhasil Ditemukan');
    }

    public function get_item()
    {
        $item = ItemVendors::where('stok', '>', 0)->get();
        return $this->successResponse($item, 'Data Berhasil Ditemukan');
    }

    public function datatablesItem($id, Request $request)
    {
        $reservasi_item = HistoryItemService::whereNotNull('id_barang')->with('barang')->where('id_reservasi', $id)->get();
        return $this->successResponse($reservasi_item, 'Data Berhasil Ditemukan');
    }

    public function generateOrderCode()
    {
        $q          = Reservation::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('m-d-Y');
        $number     = 1; //format
        $new_number = sprintf("%03s", $number);
        $code       = 'INV-' . ($new_number) . ($separator) .  $date;
        $order_code   = $code;
        if ($q > 0) {
            $last     = Reservation::orderBy('id', 'desc')->value('code_invoice');
            $last     = explode("-", $last);
            $order_code = 'INV-' . (sprintf("%03s", $last[1] + 1)) . ($separator) . $date ;
        }
        return $order_code;
    }

    public function storeReservasi(Request $request)
    {
        $findItemPaket = Package::with('get_layanan')->where('id', $request->id_paket)->first();
        try{
            $saveUser = Customer::create([
                'nama' => $request->nama,
                'telepon' => $request->telepon,
            ]);

            $saveReservasi = Reservation::create([
                'id_customer' => $saveUser->id,
                'id_pembayaran' => '-',
                'id_voucher' => '-',
                'checkin' => $request->checkin,
                'checkout' => $request->checkout,
                'berapa_malam' => $request->berapa_malam,
                'note' => $request->note,
                'id_paket' => $request->id_paket,
                'pax' => $request->pax,
                'code_invoice' => $this->generateOrderCode(),
            ]);

            $createPaket = HistoryItemService::create([
                'id_barang' => $findItemPaket->get_layanan->id,
                'id_reservasi' => $saveReservasi->id,
                'id_customer' => $saveUser->id,
                'tgl_penggunaan' => '-',
                'tgl_pengembalian' => '-',
                'status' => 'Belum Diantar',
                'qty' => 1,
                'is_include_paket' => 1,
            ]);

            $get_qty = $request->qty;
            $get_paket = $request->unit_id;

            
            foreach ($get_qty as $key => $value) {
                if(!empty($get_qty)){
                    HistoryItemService::create([
                        'id_reservasi' => $saveReservasi->id,
                        'id_customer' => $saveUser->id,
                        'status' => 'Belum Diantar',
                        'qty' => $get_qty[$key],
                        'id_barang' => $get_paket[$key],
                        'tgl_penggunaan' => '-',
                        'tgl_pengembalian' => '-',
                        'is_include_paket' => 0,
                    ]);
                }
            }
            DB::commit();
            return $this->successResponse($saveReservasi, 'Data Berhasil Disimpan');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function addItem(Request $request)
    {
        // return $request->all();
        $findItemPaket = Reservation::where('id', $request->id_reservasi)->first();
        try{
            $get_qty = $request->qty;
            $get_paket = $request->unit_id;

            if(!empty($get_qty)){
                foreach ($get_qty as $key => $value) {
                    HistoryItemService::create([
                        'id_customer' => $findItemPaket->id_customer,
                        'id_reservasi' => $findItemPaket->id,
                        'status' => 'Belum Diantar',
                        'qty' => $get_qty[$key],
                        'id_barang' => $get_paket[$key],
                        'tgl_penggunaan' => '-',
                        'tgl_pengembalian' => '-',
                    ]);
                }
            }
            DB::commit();
            return $this->successResponse($findItemPaket, 'Data Berhasil Disimpan');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteItem($id)
    {
        $deleteItem = HistoryItemService::where('id', $id)->delete();
        return $this->successResponse($deleteItem, 'Data Berhasil Dihapus');
    }
}
