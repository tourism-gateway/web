<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\BussinessRequest;
use Illuminate\Http\Request;
use App\Models\Bussiness;
use Auth;

class ApiBussinessController extends ApiController
{
    public function index(Request $request)
    {
        
        $keyword = $request->search;

        $query = Bussiness::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('nama_bisnis', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(BussinessRequest $request)
    {
        $resp = Bussiness::create([
            'nama_bisnis' => $request->nama_bisnis,
            // 'company_id' => Auth::user()->company_id,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'alamat' => $request->alamat,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Bussiness::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = Bussiness::where('id', $id)->firstOrFail();

        $row->update([
            'nama_bisnis' => $request->nama_bisnis,
            // 'company_id' => Auth::user()->company_id,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'alamat' => $request->alamat,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Bussiness::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
