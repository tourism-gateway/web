<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
// use App\Http\Requests\BussinessRequest;
use Illuminate\Http\Request;
use App\Models\Kepegawaian;
use Auth;

class ApiPegawaiController extends ApiController
{
    public function index(Request $request)
    {
        
        $keyword = $request->search;

        $query = Kepegawaian::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        // return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
        return response()->json($this->bootstrapTableFormat($query, $keyword), 200);
    }

    

    public function store(Request $request)
    {
        $resp = Kepegawaian::create([
            'bussiness_id' => $request->bussiness_id,
            // 'company_id' => Auth::user()->company_id,
            'user_id' => $request->user_id,
            'name' => $request->name,
            'phone' => $request->phone,
            'divisi' => $request->divisi,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Kepegawaian::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = Kepegawaian::where('id', $id)->firstOrFail();

        $row->update([
            'bussiness_id' => $request->bussiness_id,
            // 'company_id' => Auth::user()->company_id,
            'user_id' => $request->user_id,
            'name' => $request->name,
            'phone' => $request->phone,
            'divisi' => $request->divisi,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Kepegawaian::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
