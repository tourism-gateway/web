<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\Company;

class ApiCompaniesController extends ApiController
{
    public function index(Request $request)
    {
        
        $keyword = $request->search;

        $query = company::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $resp = company::create([
            'name' => $request->name,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = company::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = company::where('id', $id)->firstOrFail();

        $row->update([
            'name' => $request->name,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = company::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
