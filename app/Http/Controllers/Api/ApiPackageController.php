<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
// use App\Http\Requests\BussinessRequest;
use Illuminate\Http\Request;
use App\Models\Package;
use Auth;

class ApiPackageController extends ApiController
{
    public function index(Request $request)
    {
        
        $keyword = $request->search;

        $query = Package::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('nama_paket', 'like', '%' . $keyword . '%');
            }
        });

        // return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
        return response()->json($this->bootstrapTableFormat($query, $keyword), 200);
    }

    

    public function store(Request $request)
    {
        $resp = Package::create([
            'id_bisnis' => $request->id_bisnis,
            // 'company_id' => Auth::user()->company_id,
            'harga' => $request->harga,
            'nama_paket' => $request->nama_paket,
            'id_item_pelayanan' => $request->id_item_pelayanan,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Package::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = Package::where('id', $id)->firstOrFail();

        $row->update([
            'id_bisnis' => $request->id_bisnis,
            // 'company_id' => Auth::user()->company_id,
            'harga' => $request->harga,
            'nama_paket' => $request->nama_paket,
            'id_item_pelayanan' => $request->id_item_pelayanan,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Package::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
