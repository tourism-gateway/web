<?php

namespace App\Http\Requests;

use App\Models\Member;
use App\Models\Position;
use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editRuleName = $id == null ? '' : ',name,' . $id;
        $editRuleCode = $id == null ? '' : ',code,' . $id;

        return [
            'name' => 'required|unique:members' . $editRuleName,
            'code' => 'required|unique:members' . $editRuleCode,
            'branch' => 'required',
            'position_id' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        $validationInstance = $this;
        // $valida
        $validator->after(function ($validator) use ($validationInstance) {
            $manager = null;
            if ($this->manager_id != null) {
                $manager = Member::with('position')->where('id', $validationInstance->manager_id)->first();
            }

            $posotion = Position::where('id', $validationInstance->position_id)->first();
            $posisi_terendah = Position::orderBy('level', 'asc')->first();
            if ($posotion == null) {
                $validator->errors()->add('position_id', 'position not found');
            }

            if ($posotion->level == $posisi_terendah->level && $this->manager_id == null) {
                $validator->errors()->add('manager_id', 'manager is required for lowest position');
            }

            if ($posotion->level != $posisi_terendah->level && $this->manager_id != null) {
                $id = request()->route('id');
                if ($id != null) {
                    if ($id != $manager->id) {
                        if ($posotion->level >= $manager->position->level) {
                            $validator->errors()->add('manager_id', 'please select manager with height level');
                        }
                    }
                } else {
                    if ($posotion->level >= $manager->position->level) {
                        $validator->errors()->add('manager_id', 'please select manager with lower level');
                    }
                }
            }
        });
    }
}
