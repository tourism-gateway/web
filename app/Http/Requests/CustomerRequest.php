<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editRule = $id == null ? '' : ',name,' . $id;

        return [
            'name' => 'required|unique:customers' . $editRule,
            'email' => 'max:100',
            'code' => 'max:100',
            'phone' => 'max:100',
            'fax' => 'max:100',
            'no_id' => 'max:100',
            'city_name' => 'max:100',
            'no_npwp' => 'max:100',
            'rekening_nomor' => 'max:100',
            'rekening_nama' => 'max:100',
            'bank_name' => 'max:100',
        ];
    }
}
