<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BussinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editBisnis = $id == null ? '' : ',nama_bisnis,' . $id;
        $editLat = $id == null ? '' : ',latitude,' . $id;
        $editLong = $id == null ? '' : ',longitude,' . $id;
        $editAlamat = $id == null ? '' : ',alamat,' . $id;

        return [
            'nama_bisnis' => 'required' . $editBisnis,
            'latitude' => 'required' . $editLat,
            'longitude' => 'required' . $editLong,
            'alamat' => 'required' . $editAlamat,
        ];
    }
}
