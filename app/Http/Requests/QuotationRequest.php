<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editRule = $id == null ? '' : ',quotation_no_bill,' . $id;

        return [
            'customer_id' => 'required',
            'quotation_b2b' => 'required',
            'quotation_bill_to' => 'required',
            'quotation_transaction_type' => 'required',
            'quotation_date' => 'required',
            'quotation_no_bill' => 'required|unique:quotations' . $editRule,
            'quotation_ppn_persen' => 'required',
            'quotation_diskon' => 'required',
            'quotation_total_payment' => 'required',
            'detail_id' => 'required|array',
            'detail_qty' => 'required|array',
            'detail_dsikon' => 'required|array',
            'detail_type' => 'required|array',
        ];
    }
}
