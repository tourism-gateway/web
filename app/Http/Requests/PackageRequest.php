<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editBisnis = $id == null ? '' : ',id_bisnis,' . $id;
        $editHarga = $id == null ? '' : ',harga,' . $id;
        $editPaket = $id == null ? '' : ',nama_paket,' . $id;
        $editItem = $id == null ? '' : ',id_item_pelayanan,' . $id;
        
        return [
            'id_bisnis' => 'required' . $editBisnis,
            'harga' => 'required|numeric' . $editHarga,
            'nama_paket' => 'required' . $editPaket,
            'id_item_pelayanan' => 'required' . $editItem,
        ];
    }
}
