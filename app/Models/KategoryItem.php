<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoryItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'kategory_name',
    ];
}
