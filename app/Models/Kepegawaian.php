<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kepegawaian extends Model
{
    use HasFactory;
    protected $fillable = [
        'company_id',
        'bussiness_id',
        'user_id',
        'name',
        'phone',
        'divisi',
    ];
}
