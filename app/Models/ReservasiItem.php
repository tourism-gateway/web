<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservasiItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'produk_name',
        'qty',
        'price',
        'id_reservasi',
        'sub_total'
    ];
}
