<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_customer',
        'id_pembayaran',
        'id_voucher',
        'checkin',
        'checkout',
        'berapa_malam',
        'note',
        'id_paket',
        'code_invoice',
        'pax',
    ];

    public function user()
    {
        return $this->belongsTo(Customer::class, 'id_customer', 'id');
    }

    public function paket()
    {
        return $this->belongsTo(Package::class, 'id_paket', 'id');
    }

    public function detail_reservasi()
    {
        return $this->hasMany(HistoryItemService::class, 'id_reservasi');
    }
}
