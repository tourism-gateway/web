<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Service\User\UserCreateService;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'name' => 'test'
        ]);

        $userCreateService = new UserCreateService;

        $user = $userCreateService->create([
            'name' => 'admin',
            'company_id' => Company::first()->id,
            'email' => 'admin@admin.com',
            'password' => 'password',
        ]);

        $userManager = $userCreateService->create([
            'name' => 'manager-area',
            'company_id' => Company::first()->id,
            'email' => 'manager-area@admin.com',
            'password' => 'password',
        ]);

        $userLanding = $userCreateService->create([
            'name' => 'landing-page',
            'company_id' => Company::first()->id,
            'email' => 'landing-page@admin.com',
            'password' => 'password',
        ]);

        $userCS = $userCreateService->create([
            'name' => 'customer-service-finance',
            'company_id' => Company::first()->id,
            'email' => 'customer-service-finance@admin.com',
            'password' => 'password',
        ]);

        $userVendor = $userCreateService->create([
            'name' => 'vendor',
            'company_id' => Company::first()->id,
            'email' => 'vendor@admin.com',
            'password' => 'password',
        ]);

        $userWR = $userCreateService->create([
            'name' => 'warehouse',
            'company_id' => Company::first()->id,
            'email' => 'warehouse@admin.com',
            'password' => 'password',
        ]);

        $userCheck = $userCreateService->create([
            'name' => 'checker',
            'company_id' => Company::first()->id,
            'email' => 'checker@admin.com',
            'password' => 'password',
        ]);

        $userVisitor = $userCreateService->create([
            'name' => 'pengunjung',
            'company_id' => Company::first()->id,
            'email' => 'pengunjung@admin.com',
            'password' => 'password',
        ]);

        $userPIC = $userCreateService->create([
            'name' => 'pic-event-gathering',
            'company_id' => Company::first()->id,
            'email' => 'pic-event-gathering@admin.com',
            'password' => 'password',
        ]);

        $userMarketing = $userCreateService->create([
            'name' => 'marketing',
            'company_id' => Company::first()->id,
            'email' => 'marketing@admin.com',
            'password' => 'password',
        ]);

        $this->permissions();
        $this->roles();

        $user->assignRole(Role::where('name', 'admin')->firstOrFail());
        $userManager->assignRole(Role::where('name', 'manager-area')->firstOrFail());
        $userLanding->assignRole(Role::where('name', 'landing-page')->firstOrFail());
        $userCS->assignRole(Role::where('name', 'customer-service-finance')->firstOrFail());
        $userVendor->assignRole(Role::where('name', 'vendor')->firstOrFail());
        $userWR->assignRole(Role::where('name', 'warehouse')->firstOrFail());
        $userCheck->assignRole(Role::where('name', 'checker')->firstOrFail());
        $userVisitor->assignRole(Role::where('name', 'pengunjung')->firstOrFail());
        $userPIC->assignRole(Role::where('name', 'pic-event-gathering')->firstOrFail());
        $userMarketing->assignRole(Role::where('name', 'marketing')->firstOrFail());
    }

    public function permissions()
    {
        $moduls = [
            'user', 'role', 'permission', 'bussiness',

            'company', 'employes', 'packages',

            'DASHBOARD',
            
            'RESERVASI', 'INVOICE', 'VENDOR', 'VENDOR-ITEM', 'PRODUK-VENDOR', 'INVENTORY',
            'PAKET', 'LAPORAN-A3', 'LEVEL', 'PEGAWAI', 'CUSTOMER',
            'KATEGORI-CAMPSITE', 'CAMPSITE', 'SEUP-FEE', 'LANDING-PAGE-AND-NEWS-CIS',
            'VOUCHER', 'PROFILE-USER',

            'LAPORAN-HARIAN', 'LAPORAN-MINGGUAN', 'LAPORAN-BULANAN',

            'HISTORY-PRODUK', 'LAPORAN-VENDOR',

            'HISTORY-PELAYANAN', 'ADDITIONAL-PAKET-OR-PRODUK',

            'PEMBAYARAN',

            'RESERVASI-GATHERING'
        ];
        $results = [];
        foreach ($moduls as $modul) {
            $results[] = ['name' => $modul . '.show', 'guard_name' => 'web'];
            $results[] = ['name' => $modul . '.create', 'guard_name' => 'web'];
            $results[] = ['name' => $modul . '.edit', 'guard_name' => 'web'];
            $results[] = ['name' => $modul . '.delete', 'guard_name' => 'web'];
        }

        Permission::insert($results);
    }

    public function roles()
    {
        $permissions = Permission::all();

        // manager
        $permissionsManagerAreamModuls = [
            'RESERVASI', 'INVOICE', 'VENDOR', 'PRODUK-VENDOR', 'INVENTORY',
            'PAKET', 'LAPORAN-A3', 'user', 'role', 'LEVEL', 'PEGAWAI', 'CUSTOMER',
            'KATEGORI-CAMPSITE', 'CAMPSITE', 'SEUP-FEE', 'LANDING-PAGE-AND-NEWS-CIS',
            'VOUCHER', 'PROFILE-USER',
        ];
        $permissionsManagerAreamModulsAkses = [];
        foreach ($permissionsManagerAreamModuls as $modul) {
            $permissionsManagerAreamModulsAkses[] = $modul . '.show';
            $permissionsManagerAreamModulsAkses[] = $modul . '.create';
            $permissionsManagerAreamModulsAkses[] = $modul . '.edit';
            $permissionsManagerAreamModulsAkses[] = $modul . '.delete';
        }
        $permissionsManagerArea = Permission::whereIn('name', $permissionsManagerAreamModulsAkses)->get();

        // landing page
        $permissionsLandingPage = Permission::whereIn('name', ['LANDING-PAGE-AND-NEWS-CIS'])->get();

        // CS
        $permissionsCustomerServiceModuls = [
            'RESERVASI', 'INVOICE', 'VOUCHER',
            'LAPORAN-HARIAN', 'LAPORAN-MINGGUAN', 'LAPORAN-BULANAN', 'PROFILE-USER'
        ];
        $permissionsCustomerServiceModulsAkses = [];
        foreach ($permissionsCustomerServiceModuls as $modul) {
            switch ($modul) {
                case 'VOUCHER':
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.show';
                    break;

                default:
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.show';
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.create';
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.edit';
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.delete';
                    break;
            }
        }
        $permissionsCustomerService = Permission::whereIn('name', $permissionsCustomerServiceModulsAkses)->get();

        // vendor
        $permissionsVendorModuls = [
            'PRODUK-VENDOR', 'INVENTORY',
            'HISTORY-PRODUK', 'LAPORAN-VENDOR', 'LAPORAN-BULANAN',
        ];
        $permissionsVendorModulsAkses = [];
        foreach ($permissionsVendorModuls as $modul) {
            switch ($modul) {
                case 'LAPORAN-VENDOR':
                    $permissionsVendorModulsAkses[] = $modul . '.show';
                    break;

                default:
                    $permissionsVendorModulsAkses[] = $modul . '.show';
                    $permissionsVendorModulsAkses[] = $modul . '.create';
                    $permissionsVendorModulsAkses[] = $modul . '.edit';
                    $permissionsVendorModulsAkses[] = $modul . '.delete';
                    break;
            }
        }
        $permissionsVendor = Permission::whereIn('name', $permissionsVendorModulsAkses)->get();

        // warehouse
        $permissionsWarehouseModuls = [
            'INVENTORY', 'HISTORY-PRODUK', 'PROFILE-USER'
        ];
        $permissionsWarehouseModulsAkses = [];
        foreach ($permissionsWarehouseModuls as $modul) {
            $permissionsWarehouseModulsAkses[] = $modul . '.show';
            $permissionsWarehouseModulsAkses[] = $modul . '.create';
            $permissionsWarehouseModulsAkses[] = $modul . '.edit';
            $permissionsWarehouseModulsAkses[] = $modul . '.delete';
        }
        $permissionsWarehouse = Permission::whereIn('name', $permissionsWarehouseModulsAkses)->get();

        // cheker
        $permissionsCheckerModuls = [
            'HISTORY-PELAYANAN', 'ADDITIONAL-PAKET-OR-PRODUK', 'PROFILE-USER'
        ];
        $permissionsCheckerModulsAkses = [];
        foreach ($permissionsCheckerModuls as $modul) {
            switch ($modul) {
                case 'HISTORY-PELAYANAN':
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    $permissionsCheckerModulsAkses[] = $modul . '.create';
                    $permissionsCheckerModulsAkses[] = $modul . '.edit';
                    break;

                case 'ADDITIONAL-PAKET-OR-PRODUK':
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    $permissionsCheckerModulsAkses[] = $modul . '.create';
                    $permissionsCheckerModulsAkses[] = $modul . '.edit';
                    break;

                default:
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    $permissionsCheckerModulsAkses[] = $modul . '.create';
                    $permissionsCheckerModulsAkses[] = $modul . '.edit';
                    $permissionsCheckerModulsAkses[] = $modul . '.delete';
                    break;
            }
        }
        $permissionsChecker = Permission::whereIn('name', $permissionsCheckerModulsAkses)->get();

        // pengunjung
        $permissionsPengunjungModuls = [
            'HISTORY-PELAYANAN', 'ADDITIONAL-PAKET-OR-PRODUK', 'PROFILE-USER', 'RESERVASI', 'INVOICE', 'PEMBAYARAN'
        ];
        $permissionsPengunjungModulsAkses = [];
        foreach ($permissionsPengunjungModuls as $modul) {
            switch ($modul) {
                case 'HISTORY-PELAYANAN':
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    break;

                case 'INVOICE':
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    break;

                case 'PEMBAYARAN':
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    break;

                case 'ADDITIONAL-PAKET-OR-PRODUK':
                    $permissionsCheckerModulsAkses[] = $modul . '.show';
                    $permissionsCheckerModulsAkses[] = $modul . '.create';
                    $permissionsCheckerModulsAkses[] = $modul . '.edit';
                    break;

                default:
                    $permissionsPengunjungModulsAkses[] = $modul . '.show';
                    $permissionsPengunjungModulsAkses[] = $modul . '.create';
                    $permissionsPengunjungModulsAkses[] = $modul . '.edit';
                    $permissionsPengunjungModulsAkses[] = $modul . '.delete';
                    break;
            }
        }
        $permissionsPengunjung = Permission::whereIn('name', $permissionsPengunjungModuls)->get();

        // pic-event-gathering
        $permissionsPicEventGatheringModuls = [
            'RESERVASI-GATHERING', 'VOUCHER',
        ];
        $permissionsPicEventGatheringModulsAkses = [];
        foreach ($permissionsPicEventGatheringModuls as $modul) {
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.show';
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.create';
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.edit';
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.delete';
        }
        $permissionsPicEventGathering = Permission::whereIn('name', $permissionsPicEventGatheringModulsAkses)->get();

        // marketing
        $permissionsMarketingModuls = [
            'RESERVASI', 'PROFILE-USER',
        ];
        $permissionsMarketingModulsAkses = [];
        foreach ($permissionsMarketingModuls as $modul) {
            $permissionsMarketingModulsAkses[] = $modul . '.show';
            $permissionsMarketingModulsAkses[] = $modul . '.create';
            $permissionsMarketingModulsAkses[] = $modul . '.edit';
            $permissionsMarketingModulsAkses[] = $modul . '.delete';
        }
        $permissionsMarketing = Permission::whereIn('name', $permissionsMarketingModulsAkses)->get();

        Role::create([
            'name' => 'admin',
            'guard_name' => 'web',
        ])->givePermissionTo($permissions);

        Role::create([
            'name' => 'manager-area',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsManagerArea);

        Role::create([
            'name' => 'landing-page',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsLandingPage);

        Role::create([
            'name' => 'customer-service-finance',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsCustomerService);

        Role::create([
            'name' => 'vendor',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsVendor);

        Role::create([
            'name' => 'warehouse',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsWarehouse);

        Role::create([
            'name' => 'checker',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsChecker);

        Role::create([
            'name' => 'pengunjung',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsPengunjung);

        Role::create([
            'name' => 'pic-event-gathering',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsPicEventGathering);

        Role::create([
            'name' => 'marketing',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsMarketing);
    }
}
